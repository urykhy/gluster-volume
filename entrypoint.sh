#!/bin/bash

glusterd --no-daemon --log-file=/dev/stdout &
/volume.py &

trap clean_exit EXIT
function clean_exit {
  echo "cleanup..."
  local pids=$(jobs -p)
  if [[ "$pids" != "" ]]; then
    kill $pids || :
  fi
  wait $pids
}
wait
