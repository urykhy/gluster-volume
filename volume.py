#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:et
# -*- coding: utf-8 -*-

# just games. no configuration, no correct restart, and lot of hardcode.
# license: feel free to use, reuse and abuse code in this file


import string
import cherrypy
import os
import subprocess
import shlex
from cherrypy._cpcompat import json_decode

class Volume(object):

    # docker do not pass content-type, so we must workaround - @cherrypy.tools.json_in()
    def get_json(self):
        body = cherrypy.request.body.read()
        cherrypy.serving.request.json = json_decode(body.decode('utf-8'))

    def __init__(self):
        self.mounts={}
        self.servers=["gs1", "gs2", "gs3"]
        self.mount_server="gs1"
        self.mount="/var/run/docker/gluster/"   # must be `/` at end
        if not os.path.exists(self.mount):
            os.mkdir(self.mount)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def Plugin_Activate(self, **params):
        cherrypy.log("Activate")
        v={}
        v["Implements"] = ["VolumeDriver"]
        return v

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Create(self, **params):
        self.get_json()
        result={}
        volume_name = cherrypy.request.json['Name']
        cherrypy.log("Create : " + volume_name)

        options="replica 3 arbiter 1 "
        for x in self.servers:
            options += x + ":/var/lib/glusterd/brick-" + volume_name + " "

        cmd=["gluster volume create {} " + options + " force"
           , "gluster volume start {}"]

        #   , "echo y | gluster volume set {} nfs.disable off"
        #   , "gluster volume set {} nfs.export-volumes on"

        try:
            for x in cmd:
                xcmd = x.format(volume_name)
                cherrypy.log("Create cmd: " + xcmd)
                subprocess.check_call(xcmd, shell=True)
            result["Err"] = ""
        except BaseException as e:
            result["Err"] = str(e)

        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Remove(self, **params):
        result={}
        self.get_json()
        volume_name = cherrypy.request.json['Name']
        cherrypy.log("Remove : " + volume_name)

        cmd = ["echo y | gluster volume stop {}"
             , "echo y | gluster volume delete {}"]

        try:
            for x in cmd:
                xcmd = x.format(volume_name)
                cherrypy.log("Remove cmd: " + xcmd)
                subprocess.check_call(xcmd, shell=True)
            result["Err"] = ""
        except BaseException as e:
            result["Err"] = str(e)

        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Mount(self, **params):
        result={}
        self.get_json()

        volume_name = cherrypy.request.json['Name']
        id = cherrypy.request.json.get('Id', volume_name)
        cherrypy.log("Mount : " + volume_name + " as " + id)

        if volume_name in self.mounts:
            result['Mountpoint'] = self.mounts[volume_name]
        else:
            os.mkdir(self.mount + id)
            subprocess.check_call(shlex.split("mount -t glusterfs " + self.mount_server +":/{} {}/{}".format(volume_name, self.mount, id)))
            self.mounts[volume_name] = self.mount + id
            result['Mountpoint'] = self.mount + id

        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Path(self, **params):
        result={}
        self.get_json()

        volume_name = cherrypy.request.json['Name']
        cherrypy.log("Path : " + volume_name)
        if volume_name in self.mounts:
            result['Mountpoint'] = self.mounts[volume_name]

        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Unmount(self, **params):
        result={}
        self.get_json()

        volume_name = cherrypy.request.json['Name']
        id = cherrypy.request.json.get('Id', volume_name)
        cherrypy.log("Umount : " + volume_name + " as " + id)

        if volume_name in self.mounts:
            subprocess.check_call(shlex.split("umount {}/{}".format(self.mount, id)))
            os.rmdir(self.mount + id)
            del self.mounts[volume_name]
            result['Err']=''
        else:
            result['Err']='not mounter'

        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_Get(self, **params):
        result={}
        self.get_json()
        volume_name = cherrypy.request.json['Name']
        cherrypy.log("Get : " + volume_name)

        r = subprocess.check_output(shlex.split("gluster volume info {}".format(volume_name))).decode()
        status={}
        for x in r.split('\n'):
            if len(x)> 0:
                try:
                    [n,v] = x.split(':', 1)
                    n = n.strip()
                    v = v.strip()
                    if len(n) > 0 and len(v) > 0:
                        status[n]=v
                except:
                    pass

        vd={}
        vd['Name'] = volume_name
        vd['Status'] = status
        if volume_name in self.mounts:
            vd['Mountpoint'] = self.mounts[volume_name]
        result['Volume'] = vd
        return result

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def VolumeDriver_List(self, **params):
        result={}
        cherrypy.log("List")
        vd=[]
        r = subprocess.check_output(shlex.split("gluster volume list")).decode()
        for volume_name in r.split('\n'):
            if len(volume_name) > 0:
                vi = {}
                vi["Name"] = volume_name
                if volume_name in self.mounts:
                    vi['Mountpoint'] = self.mounts[volume_name]
                vd.append(vi)
        result['Volumes'] = vd
        return result

if __name__ == '__main__':
    cherrypy.config.update({'server.socket_file': '/run/docker/plugins/gluster.sock'})
    #cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    cherrypy.quickstart(Volume())
