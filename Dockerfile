FROM urykhy/gluster

RUN    apt-get update \
    && apt-get install -y python3-pip \
    && pip3 install cherrypy          \
    && rm -rf /var/lib/apt/lists/*

ADD entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh

ADD volume.py /volume.py
RUN chmod a+x /volume.py

ENTRYPOINT ["/entrypoint.sh"]
